<?php


namespace Ucc\Services;


use JsonMapper;
use KHerGe\JSON\JSON;
use Ucc\Models\Question;

class QuestionService
{
    const QUESTIONS_PATH = __DIR__ . '/../../questions.json';

    private JSON $json;
    private JsonMapper $jsonMapper;
    private static $questions = [];

    public function __construct(JSON $json, JsonMapper $jsonMapper)
    {
        $this->json = $json;
        $this->jsonMapper = $jsonMapper;

        if (empty(self::$questions)) {
          $this->jsonMapper->mapArray(
            $this->json->decodeFile(self::QUESTIONS_PATH, true),
            self::$questions,
            Question::class
          );
        }
    }

  /**
   *
   * @param int $count
   *
   * @return array
   */
    public function getRandomQuestions(int $count = 5): array
    {
      $pickedKeys = array_rand(self::$questions, $count);
      $result = [];
      foreach ($pickedKeys as $key) {
        $result[] = self::$questions[$key];
      }
      return $result;
    }

  /**
   * @param int $id
   * @param string $answer
   *
   * @return int
   */
    public function getPointsForAnswer(int $id, string $answer): int
    {
      $matches = array_filter(
        self::$questions,
        function(Question $question) use($id, $answer) {
          return
            $question->getId() === $id &&
            strtolower($question->getCorrectAnswer()) === strtolower($answer);
        }
      );
      if (count($matches)) {
        /** @var Question[] $matches */
        return $matches[0]->getPoints();
      }
      return 0;
    }
}